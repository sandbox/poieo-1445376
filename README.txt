CiviCRM's administration menu has a very different look than the popular Administration Menu module. This module overrides CiviCRM's default styling making the CiviCRM administration menu look more like the Administration Menu.

This is currently available for Drupal 7 only.

This has not been thoroughly tested across all browsers.

Dependencies:
Administration Menu
CiviCRM

Installation:
1. Upload to your site's modules directory.
2. Navigate to admin/modules and enable the module.
3. Navigate to admin/config/administration/admin_menu and make a selection for your CiviCRM menu style: none, default, or toolbar.